%define debug_package %{nil}

Name:           fd
Version:        9.0.0
Release:        1%{?dist}
Summary:        fd is a program to find entries in your filesystem.

License:        MIT AND Apache-2.0
URL:            https://github.com/sharkdp/fd
Source0:        https://github.com/sharkdp/%{name}/archive/refs/tags/v%{version}.tar.gz#/%{name}-%{version}.tar.gz

BuildRequires:  cargo
BuildRequires:  rust
BuildRequires:  rust-std-static   

%description
It is a simple, fast and user-friendly alternative to find. While it does not aim to support all of find's powerful functionality, it provides sensible (opinionated) defaults for a majority of use cases.

%prep
%autosetup -n %{name}-%{version} -p1
mkdir cargo-home
cat >cargo-home/config <<EOF
[source.crates-io]
replace-with = 'mirror'

[source.mirror]
registry = "https://mirrors.tuna.tsinghua.edu.cn/git/crates.io-index.git"
EOF


%build
export CARGO_HOME=$PWD/cargo-home
cargo build --release


%install
install -pDm755 target/release/fd %{buildroot}%{_bindir}/fd

%files
%license LICENSE-MIT LICENSE-APACHE
%{_bindir}/fd

%changelog
* Sat Jun 22 2024 yangpanda <gnaymuyi@163.com>
